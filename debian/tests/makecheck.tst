## debian/tests/makecheck.tst -- GAP Test script
## script format: GAP Reference Manual section 7.9 Test Files (GAP 4r8)
##
gap> TestPackageAvailability( "laguna" , "=3.9.7" , true );
"/usr/share/gap/pkg/laguna/"

##
## eos
